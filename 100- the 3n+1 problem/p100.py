from sys import stdin,stdout
cache = {}
def algorithm(n):
    count = 1
    nOriginal = n
    if n in cache:
        count = cache[n]
    else:
        while n != 1:
            if n%2 == 1:
                n = (n*3)+1
                n //= 2
                count+=2
            else:
                n //= 2
                count+=1
        cache[nOriginal] = count
    return count             

def nCyclesBetwAB(a,b):
    bigNumber=0
    if a>b:
        a,b = b,a
    for i in range(a,(b+1)):
        if algorithm(i) > bigNumber:
            bigNumber = algorithm(i)
    return bigNumber

if __name__ == "__main__":
    
    arryLimits = stdin.read().splitlines()
    for limits in arryLimits:
        m,n = map(int, limits.split()[:2])
        print("{} {} {}".format(m, n, nCyclesBetwAB(m,n)))
    
exit(0)